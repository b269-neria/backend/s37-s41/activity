// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// This allows us to use all the routes defined in "userRoute.js"
const userRoute = require("./routes/userRoute");

const courseRoute = require("./routes/courseRoute");
// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
// localhost:4000/users
app.use("/users", userRoute);
app.use("/courses", courseRoute);

// Database connection
mongoose.connect("mongodb+srv://mailkneria:admin123@zuitt-bootcamp.dw80oi3.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open",() => console.log('Now connected to cloud database'));


// Server listening
// Will used the defined port number for the application whenever environment variable is availble to used 4000 if none is defined
// This syntax will allow flexibility when using the application or as a hosted application
app.listen(process.env.PORT || 4000,()=> console.log(`Now connected to ${process.env.PORT || 4000}`));

