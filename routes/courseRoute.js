const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
// router.post("/create", auth.verify, (req,res)=>{
// 	const userData = auth.decode(req.headers.authorization);
// 	if(userData.isAdmin === true){
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// 	} else {
// 		res.send("Admin priviledge not detected");
// 	}
// });

router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
});

router.get("/:courseId" , (req, res)=>{
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:courseId", (req,res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.patch("/:courseId/archive",auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:courseId/reactivate",auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	courseController.reactivateCourse(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;