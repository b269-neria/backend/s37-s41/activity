// s37 Activity
// const mongoose = require("mongoose");

/*
a. name - String
b. description - String
c. price - Number
d. isActive - Boolean
e. createdOn - Date
f. enrollees - Array of objects
     i. userId - String
     ii. enrolledOn - Date (Default value - new Date object)
*/

// const mongoose = require("mongoose");
// const courseSchema = new mongoose.Schema({
// 	name : {
// 		type : String,
// 		required : [true, "Course is required"]
// 	},
// 	description : {
// 		type : String,
// 		required : [true, "Description is required"]
// 	},
// 	price : {
// 		type : Number,
// 		required : [true, "Price is required"]
// 	},
// 	isActive : {
// 		type : Boolean,
// 		default : true
// 	},
// 	createdOn : {
// 		type : Date,
// 		default : new Date()
// 	},
// 	enrollees : [
// 		{
// 			userId : {
// 				type : String,
// 				required: [true, "UserId is required"]
// 			},
// 			enrolledOn : {
// 				type : Date,
// 				default : new Date() 
// 			}
// 		}
// 	]
// });
// module.exports = mongoose.model("Course", courseSchema);

// Model needed for continuation

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Course is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	enrollees : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date() 
			}
		}
	]
});

module.exports = mongoose.model("Course", courseSchema);